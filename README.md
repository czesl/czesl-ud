# czesl-ud

UD annotation of Czesl - syntactic analysis of learner language. 


## Preparing data for annotation.

1. Use `annotation1` from the czesl repository

2. Create default UD parse based on the original w-layer and on a simply corrected w-layer 

2.1 Feat needs to be modified and run from Netbeans V 8.0.1:

2.2 Edit file `feat/Czesl/iaaSupport/src/org/purl/jh/feat/iaa/Project2WPlainVerticalAction.java`

2.3. Create default UD parse based on the original w-layer

2.3.1 In `Project2WPlainVerticalAction.java`, set output format to Project2WPlainVertical.Format.CONLLU and correction to false 

2.3.2. Run `Creates a vertical using T0 tokens + T1/2 sentences boundaries` on the annotation1 folder in czesl repository

2.3.3. Move all `czesl/annotation1/*.a.xml.vert` to `czesl-ud/data/0_dummy_from_czesl_w` and rename them to `*.w.conllu` (syntactic analysis of the T0 = w layer)	

2.3.4. Analyse with udpipe:

From within the `data/0_dummy_from_czesl_w` folder, run:

```
for /r %i in (*.conllu) do c:\bin\udpipe-1.2.0-bin\bin-win32\udpipe.exe --tag --parse c:\bin\udpipe-ud-2.0-170801\czech-ud-2.0-170801.udpipe %i --outfile ../1_default_conllu_w/{}.conllu
```


2.4. Create default UD parse based on a simply corrected w-layer 

2.4.1 In `Project2WPlainVerticalAction.java`, set output format to Project2WPlainVertical.Format.CONLLU and correction to true 

2.4.2. Run `Creates a vertical using T0 tokens + T1/2 sentences boundaries` on the annotation1 folder in czesl repository

2.4.3. Move all `czesl/annotation1/*.a.xml.vert` to `czesl-ud/data/0_dummy_from_czesl_a` and rename them to `*.w.conllu` 

2.4.4. Analyse with udpipe:

From within the `data/0_dummy_from_czesl_a` folder, run:

```
for /r %i in (*.conllu) do c:\bin\udpipe-1.2.0-bin\bin-win32\udpipe.exe --tag --parse c:\bin\udpipe-ud-2.0-170801\czech-ud-2.0-170801.udpipe %i --outfile ../1_default_conllu_a/{}.conllu
```

2.5 Replace corrected forms in 1.3. by original forms from 1.2.

Run 
```
python scripts\replaceForms.py -i data\1_default_conllu_a -f data\1_default_conllu_w -o data\1_default_conllu_wa
```

3. Select data for annotation rounds

3.1. Round1 - select simple structures across all CEFR levels

3.1.1 Create CEFR level table:
```
cat czesl/annotation1/*.meta.xml | grep -P "\<(id|cz_CEF)\/?\>" > czesl-ud/data/cefr.tsv
```
Then replace xml tags by tabs and simplify (could be done by sed, but I did it interactively) 
`<id/>` and `<cz_CEF/>` replace missing values


3.1.2. Selected 50 sentences with approved labels, max len, and evenly spread over CEFR:
```
# maxLen=15
# labels=['root', 'punct', 'nsubj', 'obj', 'iobj', 'nmod', 'nummod', 'advmod', 'amod', 'aux', 'cop', 'mark', 'case', 'det', 'nummod:gov', 'det:numgov', 'dep', 'obl', 'expl:pv', 'expl:pass', 'aux:pass']
# cefr={'A1': 7, 'A1+': 7, 'A2': 7, 'A2+': 7, 'B1': 7, 'B2': 8, 'C1': 7, 'C2': 0}
```       
Run `python scripts/selectsentences.py` (version 6c28c87)

Copied all round1 files into data\2_for_annot\round1

List of Round1 files:
```
grep -P "\sround1\s" data\2_for_annot\selection.log > data\2_for_annot\round1-selection.log
```

Later files were merged into a single conllu file with all ~50 sentences. About 3 sentences 
were removed (a single bullet, instructions).

3.2. Round2 - All remaining sentences satisfying constraints of Round1. 

python scripts/selectsentences.py -r round2 -l data\2_for_annot\round1\round1-selection.log 2> data\2_for_annot\round2.log

Copied all round2 files into data\2_for_annot\round2

3.3. TODO Remaining sentences 

## Problems

* Tokenization is often wrong (due errors in transcription conversion) and definitely different from UD guidelines




