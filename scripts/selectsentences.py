import argparse
import os
import sys
import ntpath
from typing import List, Tuple

"""
Selects simple sentences for annotation (up to certain length and only with selected labels)

To arrive to the right set of labels, use the --debug flag followed by:
cat output-dir/*.conllu | grep "# Excluded" |  sort | uniq -c | sort -rn > excluded.sus 
"""

# Only sentences with all labels in this array will be output
ALLOWED_LABELS = [
    #'acl',
    #'advcl',
    'advmod',
    'advmod:emph', #
    'amod',
    #'appos',
    'aux',
    'aux:pass',
    'case',
    #'cc',
    #'ccomp',
    #'compound',
    #'conj',
    'cop',
    #'csubj',
    #'csubj:pass',
    'dep',
    'det',
    'det:numgov',
    'det:nummod', #
    'discourse', #
    'expl:pass',
    'expl:pv',
    #'fixed',
    #'flat',
    #'flat:foreign',
    'iobj',
    'mark',
    'nmod',
    'nsubj',
    'nsubj:pass',  #
    'nummod',
    'nummod:gov',
    'obj',
    'obl',
    #'orphan'
    #'parataxis',
    'punct',
    'root',
    'xcomp'  #
]


def selectSentences(filename, allowedLabels, maxLen, debug) -> List[Tuple[str,str]]:
    """
    :param filename: .conllu file to select sentences from
    :param allowedLabels: allowed labels (sentences containing other labels will be filtered out)
    :param maxLen: max sentence length (longer sentences will be filtered out)
    :param debug: Report excluded sentences with the offending label
    :return: filtered (allowed) sentences represented as a list of ids + a conllu string
    """
    output = []

    file = open(filename, encoding="utf-8")
    text = file.read()

    paras = text.split("# newpar id = ")

    docStr = paras[0] # store doc id, write out it if anything is written

    for para in paras[1:]:
        sentences = para.split("# sent_id = ")

        paraStr = "# newpar id = " + sentences[0] # store para id, write out it if anything is written

        for sentence in sentences[1:]:
            sentence = sentence.strip('\n\r')
            lines = sentence.split('\n')

            allowed = True
            problem = None

            outStr = ""
            sentenceId = lines[0]

            for word in lines[1:]:
                if len(word) > 0:
                    wordinfo = word.split("\t")
                    if int(wordinfo[0]) > maxLen:
                        allowed = False
                        problem = f'len: {int(wordinfo[0])}'
                        break

                    if wordinfo[7] not in allowedLabels:
                        allowed = False
                        problem = f'label: {wordinfo[7]} : {wordinfo[1]}'
                        break

            if allowed or debug:
                if debug and problem:
                    outStr += f"# Excluded {problem}\n"

                if docStr:
                    outStr += docStr
                    docStr = None

                if paraStr:
                    outStr += paraStr
                    paraStr = None

                outStr += "# sent_id = " # the following sentence string starts with the id of the sentence
                outStr += sentence

                output.append((sentenceId, outStr))

            # report if the sentences was output or not
            if allowed:
                print(sentenceId, "OK", sep='\t', file=sys.stderr)
            else:
                print(sentenceId, "KO", problem, sep='\t', file=sys.stderr)

    return output


PART_SIZE = 50


def parts(sentences: List[Tuple[str,str]]) -> List[List[Tuple[str,str]]]:
    counter = 1

    part = []
    for i, conllu in sentences:
        counter += 1
        if counter > PART_SIZE and conllu.startswith("# newdoc id"):
            yield part
            part = []
            counter = 1

        part.append((i, conllu))

    yield part


def filterFiles(inDir) -> List[Tuple[str, str]]:
    """
    :return: filtered (allowed) sentences from all files in the input directory represented as a list of ids + a conllu string
    """
    output = []
    # process all files in the input directory
    for file in sorted(os.listdir(inDir)):
        print("Processing file ", file, ntpath.basename(file), file=sys.stderr)

        output.extend(selectSentences(
                filename=os.path.abspath(os.path.join(args.inDir, file)),
                allowedLabels=ALLOWED_LABELS,
                maxLen=args.maxLen,
                debug=args.debug,
        ))

    return output


if __name__ == '__main__':
    partCounter = 0

    argparser = argparse.ArgumentParser(description='Select sentences for annotation, based on their complexity. Run this from repository root for defaults to work. Edit allowed labels in code')
    argparser.add_argument('-r', '--round', dest='round', help="Prefix of this round files (e.g. round1, round2, etc)")
    argparser.add_argument('-i', '--inDir', dest='inDir', default="data/1_default_conllu_wa", help="Directory with input files in the conllu format; the files must have '.w.conllu' extension")
    argparser.add_argument('-l', '--prevLog', dest='prevLog', default="data/2_for_annot\selection.log", help="File keeping track of selection of sentences into individual rounds")
    argparser.add_argument('-o', '--outDir', dest='outDir', default="data/2_for_annot/currenttest", help="Directory to put the filtered files to")
    argparser.add_argument('-n', '--maxLen', dest='maxLen', default=15, type=int, help="Max length of the sentence")
    argparser.add_argument('-d', '--debug',  dest='debug', action='store_true', help="Report excluded sentences with the offending label")
    args = argparser.parse_args()

    print(f'Processing {args.round}', file=sys.stderr)

    # create output directory if it does not exit
    if not os.path.exists(args.outDir):
        os.makedirs(args.outDir)

    # read in the list of previously dumped sentencess
    # todo
    previous = [line.rstrip('\r\n').split()[0] for line in open(args.prevLog, encoding="utf-8")]

    output = filterFiles(args.inDir)
    print(f'Number of filtered sentences (prior removing previous rounds): {len(output)}', file=sys.stderr)

    with open(os.path.join(args.outDir, f'{args.round}-selection.log'), "w", encoding="utf-8") as selectionLog:
        print(f"# round={args.round}", file=selectionLog)
        print(f"# maxLen={args.maxLen}", file=selectionLog)
        print("# labels=" + str(ALLOWED_LABELS), file=selectionLog)

        for part in parts(output):
            partCounter += 1
            partFile = f'{args.round}-part{partCounter:03d}.conllu'
            print(f'Dumping part file {partFile}', file=sys.stderr)
            with open(os.path.join(args.outDir, partFile), "w", encoding="utf-8") as w:
               for id, conllu in part:
                    if not id in previous:
                        print(id, file=selectionLog)
                        print(conllu, file=w)
                        print(file=w)
                    else:
                        print(f"{id} old", file=sys.stderr)

    print(f'Number of filtered sentences (after removing previous rounds): {len(output)}', file=sys.stderr)
