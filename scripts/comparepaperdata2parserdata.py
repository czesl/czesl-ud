import os
import difflib

def comparestrings(string1, string2):

    """
    compares two strings by matching them blockwise and prints out the parts where they differ
    :param string1:
    :param string2:
    :return: list of matches / mismatches
    """

    s = difflib.SequenceMatcher(None, string1, string2)
    matches = s.get_matching_blocks()
    for block in matches[1:]:
        print(parsedstring[block.a - 10:block.a + 10])
        print(paperstring[block.b - 10:block.b + 10])
    return(matches)

if __name__ == '__main__':
    home = os.path.expanduser('~')

    UD_DIR = home + "/czesl-ud"
    PAPER_DIR = home + "/paper-2017-ijcnlp"

    UD_DIR = "d:/projects/czels2/bb/czesl-ud"
    PAPER_DIR = "d:/projects/czels2/bb/paper-2017-ijcnlp"

    UD_PATH = UD_DIR + "/data/default_conllu"
    PAPER_PATH = PAPER_DIR + "/data/auto-pdt/all.conll"
    
    
    filelist = os.listdir(UD_PATH)
    filelist.sort()
    parsedwords = []

    for fileid in filelist:
        file = open(UD_PATH + "/"+fileid, encoding="utf-8")
        for line in file:
            if line[0].isnumeric():
                try:
                    parsedwords.append(line.split("\t")[1])
                except:
                    print(line)

    print(len(parsedwords))
    parsedstring = "".join(parsedwords)

    file = open(PAPER_PATH, encoding="utf-8")
    paperwords = []

    for line in file:
        if line[0].isnumeric():
            try:
                paperwords.append(line.split("\t")[1])
            except:
                print(line)

    print(len(paperwords))
    paperstring = "".join(paperwords)

    #few observations: paperstring doesn't contain whitespace, but parsedstring does
    print(len("".join(paperstring.split())), len(paperstring))
    print(len("".join(parsedstring.split())), len(parsedstring))

    parsedstring = "".join(parsedstring.split())

    #strings still don't match. Where's the issue? Let's look at the differences in first 25k characters
    comparestrings(parsedstring[0:25000], paperstring[0:25000])

    """
    we see that in the parsed strings, sometimes additional html characters show up
    for example:
    &lt;li&gt;Nějaktenes
    užpopsala.Nějaktenes
    The strings in the paper don't have this issue.
    We can just delete strings starting with '&'
    """
    print('&' in paperstring)

    """
    other issues:
    parsing … character as ...
    „ as "
    “ as "
    """
    parsedstring = parsedstring.replace("&lt;li&gt", "")
    parsedstring = parsedstring.replace("&lt;pr&gt;", "")
    parsedstring = parsedstring.replace("„", '"')
    parsedstring = parsedstring.replace("“", '"')
    parsedstring = parsedstring.replace("…", '...')

    print(len(paperstring))
    print(len(parsedstring))

    #strings still don't match. Where's the issue? Let's look at the differences in first 25k characters
    comparestrings(parsedstring[0:25000], paperstring[0:25000])

    """
    we can see that there's sometimes extra semicolons, for example:
    ánemánoh?;Tak.Zkusím
    nánemánoh?Tak.Zkusím
    honámětu.;1.Životopi
    ěhonámětu.1.Životopi
    věcíjsou:;prvnívěc,j
    ívěcíjsou:prvnívěc,j
    """

    print('?;' in parsedstring)
    print('?;' in paperstring)
    parsedstring = parsedstring.replace("?;", "?")

    print('.;' in parsedstring)
    print('.;' in paperstring)
    parsedstring = parsedstring.replace(".;", ".")

    print(':;' in parsedstring)
    print(':;' in paperstring)
    parsedstring = parsedstring.replace(":;", ":")


    #strings still don't match. Where's the issue? Let's look at all the remaining differences
    completematches = comparestrings(parsedstring, paperstring)

    print(len("".join(paperstring.split())), len(paperstring))
    print(len("".join(parsedstring.split())), len(parsedstring))

    """
    open issues:
    extra semicolons by themselves
    <img> tags? Not sure if these are wanted
    extra { | } type annotations in parsed file
    text itself seems to be fine!
    """

    print(len(completematches))