# coding=utf-8

import argparse
import os

def replaceForms(srcFile, formFile, outFile):
    with open(srcFile, encoding="utf-8") as srcR, \
         open(formFile, encoding="utf-8") as formR, \
         open(outFile, "w", encoding="utf-8") as w:

        for srcLine, formLine in zip(srcR.read().splitlines(), formR.read().splitlines()):
            if srcLine.startswith('#') or not srcLine.strip():
                print(srcLine, file=w)
            else:
                srcItems = srcLine.split('\t')
                form = formLine.split('\t')[1]
                outItems = [srcItems[0]] + [form] + srcItems[2:]
                print(*outItems, sep='\t', file=w)


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Replaces forms in one set of conllu files with forms from another set of conllu files')
    argparser.add_argument('-i', '--iDir', dest='srcDir', default="data/1_default_conllu_w", help="Directory with files to replace forms in; the files must be in conllu format and have '.w.conllu' extension")
    argparser.add_argument('-f', '--fDir', dest='formDir', default="data/1_default_conllu_a", help="Directory with files to replace get the new forms from; the files must be in conllu format and have '.w.conllu' extension")
    argparser.add_argument('-o', '--outDir', dest='outDir', default="data/2_for_annot/currenttest", help="Directory to put the resulting files to")
    args = argparser.parse_args()

    # create output directory if it does not exit
    if not os.path.exists(args.outDir):
        os.makedirs(args.outDir)

    for file in os.listdir(args.srcDir):
        replaceForms(
                srcFile=os.path.join(args.srcDir, file),
                formFile=os.path.join(args.formDir, file),
                outFile=os.path.join(args.outDir, file)
        )


